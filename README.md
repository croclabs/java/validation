## Simple bean validation util project.

### Usage

You first need an interface that will handle your validations.
Such an interface could look like this (The `@Validated` Annotation
is optional):

```java
@Validated
interface ValidateTest {
    void params(
            @Min(0) @Max(5) int i,
            @NotNull String s,
            @NotBlank @Name("customBlank") String blank,
            @Valid Obj obj
    );

    @Size(min = 0, max = 3)
    default String returnVal() {
        return "123456789";
    }
}
```

The `params` method validates the parameters by their respective annotations.
This means `i` has to be between 0 and 5, `s` cannot be null, `blank` cannot
be blank and will be mapped to the name `customBlank`, and `obj` will be
validated on field layer depending on the fields inside this class and their
respective annotations.

The `returnVal` method is validated on return value layer the same way the
parameters are validated. If validation is successful, the return value
will still be returned.

_This ONLY works with annotations since the java proxies cannot handle normal
classes!_

Next, you need to wrap your Validator interface by using
`CValidation.validator`. This wraps your interface inside a Java Proxy
instance so the validation annotations can be evaluated:

```java
CValidation.validator(ValidationInterface.class);
```

This can also be returned by a Spring bean configuration method:

```java
@Bean
public ValidationInterface validator() {
    return CValidation.validator(ValidationInterface.class);
}
```

### Handling fails

A failed Validation will ALWAYS throw a `ValidationException`. This object
contains information about the violations found and the parameters, as well
as violations and messages mapped by their respective context (field name 
or return).

Handling a fail could look like this:

```java
try {
    validatedClass.doStuff(1, "lorem");
} catch (ValidationException e) {
    // log error or do whatever
}
```

In a Spring web context there is not always a need to do this. You can use a
`@ControllerAdvice` to catch this exception on endpoint level to return
a dedicated error object with the information needed.

### Parameter name discovery

There are two ways how this library can discover parmeter names.

First, you can add this to your gradle build file (or do the corresponding
action in your maven POM):

```groovy
tasks.withType(JavaCompile).configureEach {
    options.compilerArgs << '-parameters'
}
```

Second, you can use the `@Name` annotation. This annotation will map
the parameters to the specified name, because without the `-parameters`
arg java will compile parameter names to arg0, arg1, arg2 and so on.

You can also combine both, to overwrite a parameter name with the `@Name`
annotation. Using the annotation, you can ALSO group errors from different
parameters by giving them the same name.