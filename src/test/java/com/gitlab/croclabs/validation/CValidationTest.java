package com.gitlab.croclabs.validation;

import com.gitlab.croclabs.validation.CValidation.Name;
import com.gitlab.croclabs.validation.CValidation.Validated;
import com.gitlab.croclabs.validation.CValidation.ValidationException;
import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.gitlab.croclabs.validation.CValidation.validator;

class CValidationTest {
	@Validated
	interface ValidateTest {
		void params(
				@Min(0) @Max(5) int i,
				@NotNull String s,
				@NotBlank @Name("customBlank") String blank,
				@Valid Obj obj
		);

		@Size(max = 3)
		default String returnVal() {
			return "123456789";
		}
	}

	@Data
	@AllArgsConstructor
	static class Obj {
		@Min(1)
		int i;

		@NotBlank
		String s;
	}

	@Test
	void validate() {
		ValidateTest validated = validator(ValidateTest.class);

		try {
			validated.params(-1, null, "", new Obj(0, ""));
		} catch (ValidationException e) {
			Assertions.assertEquals(5, e.getConstraintViolations().size());
		}

		try {
			validated.returnVal();
		} catch (ValidationException e) {
			Assertions.assertEquals(1, e.getConstraintViolations().size());
		}
	}
}