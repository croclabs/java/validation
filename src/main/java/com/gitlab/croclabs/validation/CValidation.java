package com.gitlab.croclabs.validation;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Validation;
import jakarta.validation.ValidatorFactory;
import jakarta.validation.executable.ExecutableValidator;
import lombok.Getter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.*;

public final class CValidation {

	private CValidation() {
		super();
	}

	@SuppressWarnings("unchecked")
	public static <T> T validator(Class<T> clazz) {
		return (T) Proxy.newProxyInstance(
				CValidation.class.getClassLoader(),
				new Class[]{ clazz },
				new ValidationInvocationHandler()
		);
	}

	private static class ValidationInvocationHandler implements InvocationHandler {
		ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
		ExecutableValidator ev = vf.getValidator().forExecutables();

		@Override
		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
			Set<ConstraintViolation<?>> violations = new LinkedHashSet<>();
			Map<String, Set<ConstraintViolation<Object>>> errorMap = new LinkedHashMap<>();
			Map<String, List<String>> errorMessageMap = new LinkedHashMap<>();
			ParamInfo[] info = Arrays.stream(method.getParameters()).map(p -> new ParamInfo(
					p.getName(),
					p.isAnnotationPresent(Name.class)
							? p.getAnnotation(Name.class).value()
							: null
			)).toArray(ParamInfo[]::new);

			if (args != null) {
				forParams(proxy, method, args, info, errorMap, errorMessageMap, violations);
			}

			if (method.isDefault()) {
				return forReturn(proxy, method, args, errorMap, errorMessageMap, violations);
			}

			return null;
		}

		private Object forReturn(Object proxy, Method method, Object[] args, Map<String, Set<ConstraintViolation<Object>>> errorMap, Map<String, List<String>> errorMessageMap, Set<ConstraintViolation<?>> violations) throws Throwable {
			Object arg = InvocationHandler.invokeDefault(proxy, method, args);
			Set<ConstraintViolation<Object>> foundViolations = ev.validateReturnValue(proxy, method, arg);
			errorMap.put("return", foundViolations);
			errorMessageMap.put("return", new ArrayList<>());
			foundViolations.forEach(fv -> errorMessageMap.get("return").add(fv.getMessage()));
			violations.addAll(foundViolations);

			if (!violations.isEmpty()) {
				throw new ValidationException("Validation failed", violations, null, errorMap, errorMessageMap);
			}

			return arg;
		}

		private void forParams(Object proxy, Method method, Object[] args, ParamInfo[] info, Map<String, Set<ConstraintViolation<Object>>> errorMap, Map<String, List<String>> errorMessageMap, Set<ConstraintViolation<?>> violations) {
			Set<? extends ConstraintViolation<?>> foundViolations = ev.validateParameters(proxy, method, args);

			foundViolations.forEach(fv -> processViolation(info, errorMap, errorMessageMap, fv));

			violations.addAll(foundViolations);

			if (!violations.isEmpty()) {
				throw new ValidationException("Validation failed", violations, info, errorMap, errorMessageMap);
			}
		}

		@SuppressWarnings("unchecked")
		private static void processViolation(ParamInfo[] info, Map<String, Set<ConstraintViolation<Object>>> errorMap, Map<String, List<String>> errorMessageMap, ConstraintViolation<?> fv) {
			String paramName = resolveParamName(info, fv);

			if (!errorMap.containsKey(paramName)) {
				errorMap.put(paramName, new LinkedHashSet<>());
				errorMessageMap.put(paramName, new ArrayList<>());
			}

			errorMap.get(paramName).add((ConstraintViolation<Object>) fv);
			errorMessageMap.get(paramName).add(fv.getMessage());
		}

		private static String resolveParamName(ParamInfo[] info, ConstraintViolation<?> fv) {
			String path = fv.getPropertyPath().toString();
			String name = path.split("\\.")[1];

			if (name.matches("(arg)\\d+")) {
				int index = Integer.parseInt(name.replace("arg", ""));
				return info[index].getName();
			} else {
				for (ParamInfo paramInfo : info) {
					if (paramInfo.origName().equals(name)) {
						return paramInfo.getName();
					}
				}
			}

			return name;
		}

	}

	@Target({ElementType.METHOD, ElementType.TYPE})
	@Retention(RetentionPolicy.RUNTIME)
	public @interface Validated {
		// marker annotation
	}

	@Target({ElementType.PARAMETER, ElementType.FIELD})
	@Retention(RetentionPolicy.RUNTIME)
	public @interface Name {
		String value();
	}

	@Getter
	public static class ValidationException extends ConstraintViolationException {

		private final ParamInfo[] paramInfo;
		private final Map<String, Set<? extends ConstraintViolation<?>>> errorMap = new LinkedHashMap<>();
		private final Map<String, List<String>> errorMessageMap;

		public ValidationException(
				String message,
				Set<? extends ConstraintViolation<?>> constraintViolations,
				ParamInfo[] paramInfo, Map<String, Set<ConstraintViolation<Object>>> errorMap,
				Map<String, List<String>> errorMessageMap) {
			super(message, constraintViolations);
			this.paramInfo = paramInfo;
			this.errorMap.putAll(errorMap);
			this.errorMessageMap = errorMessageMap;
		}
	}

	public record ParamInfo(String origName, String annName) {
		public String getName() {
			return annName == null ? origName : annName;
		}
	}
}
